const axios = require("axios");
const cheerio = require("cheerio");
const fs = require("fs");
const chalk = require("chalk");
const mongoose = require("mongoose");
const Book = require("./model/Book");
mongoose.Promise = global.Promise;

const url = "https://litnet.com/ru/top/litrpg?sort=latest&page=1";
const outputFile = "data.json";
const parsedResults = [];
const allPages = [];
let pageCounter = 2;
let resultCount = 0;

console.log(
  chalk.yellow.bgBlue(
    `\n  Scraping of ${chalk.underline.bold(url)} initiated...\n`
  )
);

const getWebsiteContent = async url => {
  try {
    const response = await axios.get(url);
    const $ = cheerio.load(response.data);
    // const totalPage = parseInt($("li.last a").text());
    const totalPage = 4;
    mongoose
      .connect(
        "mongodb+srv://riktos:restart987@scrap-books-npwms.mongodb.net/test?retryWrites=true&w=majority"
      )
      .then(async () => {
        $(".book-title").map((i, el) => {
          const count = resultCount++;
          const urlBook =
            "https://litnet.com" +
            $(el)
              .find("a")
              .attr("href");
          const bookLink = {
            count: count,
            url: urlBook
          };
          allPages.push(urlBook);
        });

        // Pagination Elements Link
        const nextPageLink =
          "https://litnet.com/ru/top/litrpg?sort=latest&page=" + pageCounter;
        console.log(chalk.cyan(`  Scraping: ${nextPageLink}`));

        if (pageCounter === totalPage) {
          for (const key in allPages) {
            const element = allPages[key];
            item = await scrapBookData(element);
            const bookData = new Book(item);
            parsedResults.push(bookData);
            // console.log(bookData);
            // bookData.save({});
          }
          for (const key in parsedResults) {
            await parsedResults[key].save({});
            console.log(123, parsedResults[key]);
          }
          // exportResults(parsedResults);
          return false;
        }
        pageCounter++;
        getWebsiteContent(nextPageLink);
      })
      .catch(err => {
        console.log("err", err);
      });
  } catch (error) {
    exportResults(parsedResults);
    console.error(error);
  }
};

const scrapBookData = async bookLink => {
  try {
    const response = await axios.get(bookLink);
    const $ = cheerio.load(response.data);

    const temp = {};
    temp.title = $("h1.roboto").text();
    temp.author = $("a.author").text();
    temp.url = bookLink;
    temp.image = $("div.book-view-cover img").attr("src");

    temp.desc = $("#annotation")
      .text()
      .trim()
      .replace(/\n|^.+\s{2}/gi, "")
      .trim();
    if (
      $("a.buy-btn")
        .text()
        .trim() == ""
    ) {
      temp.price = "Free";
    } else {
      temp.price =
        $("a.buy-btn")
          .text()
          .trim()
          .replace(/\D+/gim, "") + " RUB";
    }
    return temp;
  } catch (error) {
    console.log("err");
    console.log(error.response.status);
    return 2;
  }
};

const exportResults = parsedResults => {
  fs.writeFile(outputFile, JSON.stringify(parsedResults, null, 4), err => {
    if (err) {
      console.log(err);
    }
    console.log(
      chalk.yellow.bgBlue(
        `\n ${chalk.underline.bold(
          parsedResults.length
        )} Results exported successfully to ${chalk.underline.bold(
          outputFile
        )}\n`
      )
    );
  });
};

getWebsiteContent(url);
